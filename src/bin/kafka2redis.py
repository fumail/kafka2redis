#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#   Copyright 2020-2021 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import socket
import configparser
import time
import logging
import threading
import sys
import struct
import os
from fuglu.plugins.fuzor import RedisBackend as FuzorBackend
from fuglu.plugins.attachment import FileHashRedis
from fuglu.extensions.redisext import RedisPooledConn, ExpiringCounter

try:
    import six
    if sys.version_info >= (3, 12, 0): # https://github.com/dpkp/kafka-python/issues/2401#issuecomment-1760208950
        sys.modules['kafka.vendor.six.moves'] = six.moves
    # requires kafka-python
    from kafka import KafkaConsumer
except ImportError:
    pass



LOG_FORMAT_STDOUT='%(asctime)s %(name)s %(levelname)s %(message)s'



class Kafka2Redis(object):
    def __init__(self, configfile):
        self.config = configparser.ConfigParser()
        self.config.read(configfile)
        self.workers = []
        self.interval = self.config.getint('main', 'interval')
        self.event = threading.Event()
        
        logging.basicConfig(format=LOG_FORMAT_STDOUT, level=getattr(logging, self.config.get('main', 'loglevel').upper()))
        logging.getLogger('kafka').setLevel(logging.ERROR)
        self.logger = logging.getLogger(f'k2r.{self.__class__.__name__}')
    
    
    def start_workers(self):
        monitor = Monitor(self.config, self.workers)
        monitor.daemon = True
        if self.config.getint('main', 'port') > 1024:
            monitor.start()
        else:
            self.logger.info('not starting monitor thread')
        
        conns = self.config.get('main', 'connections').split()
        for conn in conns:
            kafkaid, redisid = conn.split(':', 1)
            worker = Worker(self.config, kafkaid, redisid, self.interval)
            worker.daemon = True
            self.workers.append(worker)
            worker.start()
            self.logger.info(f'started {worker.name}')
        
        try:
            # keep main thread running as worker threads are daemons
            while not self.event.is_set():
                self.event.wait(1)
        except KeyboardInterrupt:
            self.event.set()
        
        for worker in self.workers:
            self.logger.info(f'stopping {worker.name}')
            worker.stop()
        time.sleep(self.interval*2)
        for worker in self.workers:
            self.logger.info(f'terminating {worker.name}')
            worker.join(timeout=self.interval)
        monitor.stop()



class Monitor(threading.Thread):
    socket = None
    stayalive = True
    
    def __init__(self, config, workers):
        threading.Thread.__init__(self, name='monitor')
        self.config = config
        self.workers = workers
        self.logger = logging.getLogger(f'k2r.{self.__class__.__name__}')
        
    def _get_linger(self):
        linger_enabled = 0
        linger_time = 10
        linger_struct = struct.pack('ii', linger_enabled, linger_time)
        return linger_struct
        
    def _init_socket(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, self._get_linger())
        
        port = self.config.getint('main', 'port')
        self.socket.bind(('127.0.0.1', port))
        self.socket.listen()

    def run(self):
        self._init_socket()
        while self.stayalive:
            try:
                clientsocket, address = self.socket.accept()
                clientsocket.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, self._get_linger())
                MonitorHandler(clientsocket, address, self.workers).start()
            except Exception as e:
                self.logger.error(f'got error: {str(e)}')
    
    def stop(self):
        self.stayalive = False
        if self.socket is not None:
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()



class MonitorHandler(threading.Thread):
    def __init__(self, sock, address, workers):
        threading.Thread.__init__(self, name=f'monitorhandler address={address}')
        self.socket = sock
        self.workers = workers
        self.logger = logging.getLogger(f'k2r.{self.__class__.__name__}')
        
    def run(self):
        data = self.socket.recv(1024)
        data = data.strip()
        self.logger.debug(f'got message {data}')
        reply = 'invalid command'
        if data == b'lastmessage':
            reply = '%s' % int(max([w.lastmessage for w in self.workers]))
        elif data == b'errorcount':
            reply = '%s' % sum([w.get_errorcount() for w in self.workers])
        elif data == b'workers':
            rdat = [w.name for w in self.workers if w.stayalive]
            reply = '\n'.join(rdat)
        elif data == b'redisping':
            rdat = ['%s %s' % (w.name, w.redis_connection.ping()) for w in self.workers if w.redis_connection]
            reply = '\n'.join(rdat)
        elif data == b'kafkasub':
            rdat = ['%s %s' % (w.name, w.kafkaconsumer.subscription()) for w in self.workers if w.kafkaconsumer]
            reply = '\n'.join(rdat)
        elif data == b'help':
            reply = 'implemented commands:\n' + '\n'.join(['lastmessage', 'errorcount', 'workers', 'redisping', 'kafkasub', 'help'])
        reply += '\n'
        self.socket.sendall(reply.encode())
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
        except Exception as e:
            self.logger.error(f'failed to shutdown socket: {str(e)}')
        self.socket.close()
    
    
    
class Worker(threading.Thread):
    stayalive = True
    
    def __init__(self, config, kafkaid, redisid, interval):
        threading.Thread.__init__(self, name=f'worker {kafkaid}->{redisid}')
        self.logger = logging.getLogger(f'k2r.{self.__class__.__name__}.{kafkaid}.{redisid}')
        self.config = config
        self.kafkasection = f'kafka_{kafkaid}'
        self.redissection = f'redis_{redisid}'
        self.failonerr = self.config.getint(self.redissection, 'failonerr')
        self.interval = interval
        self.kafkaconsumer = None
        self.lastmessage = 0
        self.errorcount = 0
        self._init_consumer()
        self._redis_pool_conn = None
        self.backends = {}
        self._init_backends()
    
    
    def get_errorcount(self):
        c = self.errorcount
        self.errorcount = 0
        return c
        
        
    def _init_consumer(self):
        if self.kafkaconsumer is None:
            hosts = self.config.get(self.kafkasection, 'hosts').split()
            if hosts:
                clientid = 'cons-%s' % socket.gethostname()
                topic = self.config.get(self.kafkasection, 'topic')
                username = self.config.get(self.kafkasection, 'username')
                password = self.config.get(self.kafkasection, 'password')
                self.kafkaconsumer = KafkaConsumer(
                    # api_version=(0, 10, 1) will work for most kafka
                    # api_version=None should autodetect correct settings and work for most versions of kafka and redpanda
                    bootstrap_servers=hosts, api_version=None, client_id=clientid, group_id=clientid,
                    auto_offset_reset='earliest', enable_auto_commit=True, sasl_plain_username=username, sasl_plain_password=password
                )
                self.kafkaconsumer.subscribe([topic])
                
            
    def _init_redis(self):
        if self._redis_pool_conn is None:
            self._redis_pool_conn = RedisPooledConn(self.config.get(self.redissection, 'redis_conn'))
            
            
    def _get_redis(self):
        self._init_redis()
        redisconn = self._redis_pool_conn.get_conn()
        return redisconn
    
    
    def _init_backends(self):
        self.backends['fuzor'] = FuzorBackend
        self.backends['expcount'] = ExpiringCounter
        self.backends['filehash'] = FileHashRedis
    
    
    def run(self):
        while self.stayalive:
            try:
                topicpartitions = self.kafkaconsumer.poll(timeout_ms=self.interval*1000)
                for tp in topicpartitions.keys():
                    msgs = topicpartitions[tp]
                    for msg in msgs:
                        now = time.time()
                        self.lastmessage = now
                        key = msg.key or b''
                        val = msg.value or b'' # some bytes
                        age = msg.timestamp/1000-now
                        self.logger.debug(f'got msg key={key.decode()} and val={val.decode()}')
                        self.write_redis(key, val, age)
            except Exception as e:
                self.logger.error(f'error processing message: {str(e)}')
                self.logger.exception(e)
                if self.failonerr != 0 and self.failonerr > self.errorcount:
                    self.logger.info(f'stopping after too many errors')
                    self.stayalive = False
                self.errorcount =+ 1
        self.kafkaconsumer.close()
    
    
    def stop(self):
        self.stayalive = False
        self.kafkaconsumer.commit()
    
    
    def write_redis(self, key, data, age):
        datatype = self.config.get(self.redissection, 'datatype')
        if datatype == 'fuzor':
            fuzorhash = data.decode()
            self._fuzor_incr(fuzorhash, age=age)
        elif datatype == 'expcount':
            btcaddr = data.decode()
            self._expcount_incr(btcaddr, age=age)
        elif datatype == 'filehash':
            self._filehash_insert(key.decode(), data, age=age)
    
    
    def _fuzor_incr(self, digest, age=0):
        ttl = self.config.getint(self.redissection, 'ttl')
        td = int(ttl-age)
        if td <= 0:
            return
        fuzorbackendclass = self.backends.get('fuzor')
        if fuzorbackendclass is not None:
            self._init_redis()
            fuzorbackend = fuzorbackendclass(self._redis_pool_conn)
            fuzorbackend.ttl = td
            fuzorbackend.increase(digest)
        
    
    
    def _expcount_incr(self, value, age=0):
        ttl = self.config.getint(self.redissection, 'ttl')
        td = int(ttl-age)
        if td <= 0:
            self.logger.debug(f'skipping old value {value} with age {age}')
            return
        
        expcountclass = self.backends.get('expcount')
        if expcountclass:
            self._init_redis()
            backend = expcountclass(self._redis_pool_conn)
            backend.ttl = td
            backend.maxcount = self.config.getint(self.redissection, 'maxcount')
            backend.increase(value)
            #self.logger.debug(f'added value {value} with result {result[0]}')
    
    
    def _filehash_insert(self, myhash, messagebytes, age=0):
        ttl = self.config.getint(self.redissection, 'ttl')
        self._init_redis()
        redisbackend = FileHashRedis(self._redis_pool_conn, ttl, self.logger, 'k2r')
        redisbackend.insert(myhash, messagebytes, age)
    
    
if __name__ == '__main__':
    cfs = ['/k2r/kafka2redis.ini', '/etc/kafka2redis/kafka2redis.ini']
    if len(sys.argv)>1:
        cfs.insert(0, sys.argv[1])
    for file in cfs:
        if os.path.exists(file):
            cf = file
            break
    else:
        print('ERROR: no config found')
        sys.exit(1)
    
    k2r = Kafka2Redis(cf)
    k2r.start_workers()
