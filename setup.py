# -*- coding: UTF-8 -*-
import glob
import os
from setuptools import find_packages, setup

data_files = [
    ('/etc/kafka2redis',glob.glob('conf/*.dist')),
    ('/usr/lib/systemd/system/', ['systemd/kafka2redis.service']),
]



setup(
    name='python3-kafka2redis',
    version = 0.1,
    description = 'Kafka To Redis Bridge',
    author = 'Fumail Project',
    url='http://www.fuglu.org',
    author_email = 'k2r@fuglu.org',
    package_dir={'':'src'},
    packages = find_packages('src'),
    scripts=[os.path.join('src/bin/', fn) for fn in os.listdir('src/bin/')],
    long_description = 'Kafka To Redis Bridge',
    install_requires=[
        'kafka-python',
        'redis',
        'fuglu',
    ],
    data_files=data_files,
    entry_points={},
)
